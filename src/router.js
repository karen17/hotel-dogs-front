import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [
      {
        path: '',
        component: () => import('@/components/Layout.vue'),
        children: [
          {
            path: '',
            name: 'home',
            component: () => import('@/views/Home.vue')
          },
          {
            path: '/hotel/listado-mascotas',
            name: 'listado-mascotas',
            component: () => import('@/views/hotel/ListadoMascotas.vue')
          },
          {
            path: '/hotel/registrar-mascotas',
            name: 'registrar-mascotas',
            component: () => import('@/views/hotel/RegistrarMascotas.vue')
          },
          {
            path: '/hotel/asignar-mascotas',
            name: 'asignar-mascotas',
            component: () => import('@/views/hotel/AsignarMascotas.vue')
          },
          {
            path: '/hotel/retirar-mascotas',
            name: 'retirar-mascotas',
            component: () => import('@/views/hotel/RetirarMascotas.vue')
          },
          {
            path: '/hotel/habitaciones-mascotas',
            name: 'habitaciones-mascotas',
            component: () => import('@/views/hotel/HabitacionesMascotas.vue')
          },
          {
            path: '/hotel/registros-mascotas',
            name: 'registros-mascotas',
            component: () => import('@/views/hotel/RegistrosMascotas.vue')
          },
          {
            path: '/spa/banos',
            name: 'banos',
            component: () => import('@/views/spa/BañosMascotas.vue')
          },
          {
            path: '/spa/peluqueria',
            name: 'peluqueria',
            component: () => import('@/views/spa/PeluqueriaMascotas.vue')
          },
          {
            path: '/spa/manicura',
            name: 'manicura',
            component: () => import('@/views/spa/ManicureriaMascotas.vue')
          }
        ]
      },
      {
        path: '*',
        redirect: ''
      }
    ],
})



export default router
